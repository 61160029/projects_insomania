import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class QuestionWidget extends StatefulWidget {
  QuestionWidget({Key? key}) : super(key: key);

  @override
  _QuestionWidgetState createState() => _QuestionWidgetState();
}

class _QuestionWidgetState extends State<QuestionWidget> {
  var questions = [
    'นอนไม่หลับเพราะคิดมากหรือกังวลใจ',
    'รู้สึกหงุดหงิด รำคาญใจ',
    'ทำอะไรไม่ได้เลยเพราะประสาทตึงเครียด',
    'มีความวุ่นวายใจ',
    'ไม่อยากพบปะผู้คน',
    'ปวดหัวข้างเดียวหรือปวดบริเวณขมับทั้ง 2 ข้าง',
    'รู้สึกไม่มีความสุขและเศร้าหมอง',
    'รู้สึกหมดหวังในชีวิต',
    'รู้สึกว่าตนเองไม่มีคุณค่า',
    'กระวนกระวายอยู่ตลอดเวลา',
    'รู้สึกว่าตนเองไม่มีสมาธิ',
    'รู้สึกเพลียจนไม่มีแรงทำอะไร',
    'รู้สึกเบื่อหน่ายไม่อยากทำอะไร',
    'มีอาการหัวใจเต้นแรง',
    'เสียงสั่น ปากสั่น และมือสั่น เวลาไม่พอใจ',
    'รู้สึกผิดพลาดในการทำสิ่งต่างๆ',
    'หรือเกร็งกล้ามเนื้อบริเวณท้ายทอย หลัง หรือไหล่',
  ];
  var questionValues = [
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Question'),
      ),
      body: ListView(
        children: [
          CheckboxListTile(
              value: questionValues[0],
              title: Text(questions[0]),
              onChanged: (newvalue) {
                setState(() {
                  questionValues[0] = newvalue!;
                });
              }),
          CheckboxListTile(
              value: questionValues[1],
              title: Text(questions[1]),
              onChanged: (newvalue) {
                setState(() {
                  questionValues[1] = newvalue!;
                });
              }),
          CheckboxListTile(
              value: questionValues[2],
              title: Text(questions[2]),
              onChanged: (newvalue) {
                setState(() {
                  questionValues[2] = newvalue!;
                });
              }),
          CheckboxListTile(
              value: questionValues[3],
              title: Text(questions[3]),
              onChanged: (newvalue) {
                setState(() {
                  questionValues[3] = newvalue!;
                });
              }),
          CheckboxListTile(
              value: questionValues[4],
              title: Text(questions[4]),
              onChanged: (newvalue) {
                setState(() {
                  questionValues[4] = newvalue!;
                });
              }),
          CheckboxListTile(
              value: questionValues[5],
              title: Text(questions[5]),
              onChanged: (newvalue) {
                setState(() {
                  questionValues[5] = newvalue!;
                });
              }),
          CheckboxListTile(
              value: questionValues[6],
              title: Text(questions[6]),
              onChanged: (newvalue) {
                setState(() {
                  questionValues[6] = newvalue!;
                });
              }),
          CheckboxListTile(
              value: questionValues[7],
              title: Text(questions[7]),
              onChanged: (newvalue) {
                setState(() {
                  questionValues[7] = newvalue!;
                });
              }),
          CheckboxListTile(
              value: questionValues[8],
              title: Text(questions[8]),
              onChanged: (newvalue) {
                setState(() {
                  questionValues[8] = newvalue!;
                });
              }),
          CheckboxListTile(
              value: questionValues[9],
              title: Text(questions[9]),
              onChanged: (newvalue) {
                setState(() {
                  questionValues[9] = newvalue!;
                });
              }),
          CheckboxListTile(
              value: questionValues[10],
              title: Text(questions[10]),
              onChanged: (newvalue) {
                setState(() {
                  questionValues[10] = newvalue!;
                });
              }),
          ElevatedButton(
              onPressed: () async {
                await _saveQuestion();
                Navigator.pop(context);
              },
              child: const Text('Save'))
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _loadQuestion();
  }

  Future<void> _loadQuestion() async {
    var prefs = await SharedPreferences.getInstance();
    var strQuestionValue = prefs.getString('question_values') ??
        '[false, false, false, false, false, false, false, false, false, false, false,]';
    var arrStrQuestionValues =
        strQuestionValue.substring(1, strQuestionValue.length - 1).split(',');
    setState(() {
      for (var i = 0; i < arrStrQuestionValues.length; i++) {
        questionValues[i] = (arrStrQuestionValues[i].trim() == 'true');
      }
    });
  }

  Future<void> _saveQuestion() async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString('question_values', questionValues.toString());
  }
}
