import 'package:flutter/material.dart';
import 'package:covid_app/main.dart';

void main() {
  runApp(MaterialApp(
    title: "Insomania Check",
    home: StartPage(),
  ));
}

class StartPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: RaisedButton(
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => MyApp()));
        },
        child: Text("Start"),
      )),
    );
  }
}
